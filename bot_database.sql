
-- -----------------------------------------------------
-- Table `vk_bot`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vk_bot`.`users` (
  `source_id` INT(11) NOT NULL,
  `source_type` INT(11) NOT NULL,
  `user_name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`source_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vk_bot`.`audio_records`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vk_bot`.`audio_records` (
  `record_id` INT(11) NOT NULL AUTO_INCREMENT,
  `source_id` INT(11) NOT NULL,
  `file_name` VARCHAR(45) NOT NULL,
  `date_time` DATETIME NOT NULL,
  `attachment_string` VARCHAR(45) NOT NULL,
  `record_hash` BINARY(32) NULL DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  UNIQUE INDEX `record_hash` (`record_hash` ASC) VISIBLE,
  INDEX `source_id` (`source_id` ASC) VISIBLE,
  CONSTRAINT `audio_records_ibfk_1`
    FOREIGN KEY (`source_id`)
    REFERENCES `vk_bot`.`users` (`source_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vk_bot`.`grades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vk_bot`.`grades` (
  `record_id` INT(11) NOT NULL,
  `judge_id` INT(11) NOT NULL,
  `grade` TINYINT(4) NULL DEFAULT NULL,
  `send_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `grade_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`record_id`, `judge_id`),
  INDEX `judge_id` (`judge_id` ASC) VISIBLE,
  CONSTRAINT `grades_ibfk_1`
    FOREIGN KEY (`record_id`)
    REFERENCES `vk_bot`.`audio_records` (`record_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `grades_ibfk_2`
    FOREIGN KEY (`judge_id`)
    REFERENCES `vk_bot`.`users` (`source_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vk_bot`.`moderation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vk_bot`.`moderation` (
  `record_id` INT(11) NOT NULL,
  `moderator_id` INT(11) NOT NULL,
  `send_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `decision` TINYINT(4) NULL DEFAULT NULL,
  `decision_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`record_id`, `moderator_id`),
  INDEX `moderator_id` (`moderator_id` ASC) VISIBLE,
  CONSTRAINT `moderation_ibfk_1`
    FOREIGN KEY (`record_id`)
    REFERENCES `vk_bot`.`audio_records` (`record_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `moderation_ibfk_2`
    FOREIGN KEY (`moderator_id`)
    REFERENCES `vk_bot`.`users` (`source_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `vk_bot`.`rights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vk_bot`.`rights` (
  `source_id` INT(11) NOT NULL,
  `acl_type` INT(11) NOT NULL,
  `acl_rights` TINYINT(1) NOT NULL,
  PRIMARY KEY (`source_id`, `acl_type`),
  CONSTRAINT `rights_ibfk_1`
    FOREIGN KEY (`source_id`)
    REFERENCES `vk_bot`.`users` (`source_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
