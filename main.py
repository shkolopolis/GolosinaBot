from tokens import *
import json
import vk_api
import random
import time
import requests
import mysql.connector
from vk_api import VkUpload
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.utils import get_random_id

# VK Configuration
our_token = vk_token
our_group_id = '184414310'

# TG Configuration
telegram_token = tg_token


vk_session = vk_api.VkApi(token=our_token)
vk = vk_session.get_api()
longpoll = VkBotLongPoll(vk_session, our_group_id)

connection = mysql.connector.connect(host='localhost',
                                     database='vk_bot',
                                     user='root',
                                     password='root')

cursor = connection.cursor()


def save_work(event):
    # register the user if not exists
    query = "INSERT IGNORE INTO users (source_id) VALUES (%s)"
    values = (event.obj.from_id,)
    cursor.execute(query, values)
    connection.commit()

    # save a file from the voice message
    url = event.obj['attachments'][0]['audio_message']['link_ogg']
    my_file = requests.get(url)
    file_name = time.strftime("%d-%m-%Y__%H-%M-%S") + "__id" + str(event.obj['from_id']) + ".ogg"
    open(file_name, 'wb').write(my_file.content)

    # upload the file to VK server and create an attachment
    upload = vk_api.VkUpload(vk_session)
    audio_message = upload.audio_message(file_name, group_id=our_group_id)
    attachment_string = 'doc{}_{}'.format(audio_message['audio_message']['owner_id'],
                                          audio_message['audio_message']['id'])

    # add an entry to the database
    query = "INSERT INTO audio_records (source_id, file_name, date_time, attachment_string) VALUES (%s, %s, %s, %s)"
    values = (event.obj['from_id'], file_name, time.strftime('%Y-%m-%d %H:%M:%S'), attachment_string)
    cursor.execute(query, values)
    connection.commit()
    cursor.execute("SELECT LAST_INSERT_ID()")
    work_id = str(cursor.fetchall()[0][0])
    return work_id, event.obj['from_id'], attachment_string


def send_to_moderators(info_list):
    # generate the free moderators list
    moderators = []
    cursor.execute("SELECT users.source_id FROM users INNER JOIN rights ON users.source_id = rights.source_id "
                   "WHERE acl_type = 1 AND acl_rights = 1 AND users.source_id NOT IN "
                   "(SELECT moderator_id FROM moderation WHERE decision IS NULL);")
    moderator_rows = cursor.fetchall()
    for entry in moderator_rows:
        moderators.append(entry[0])
    # send for all moderators
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Одобрить', color=VkKeyboardColor.POSITIVE, payload=['moderate', info_list[0], 'approve'])
    keyboard.add_button('Блокировать', color=VkKeyboardColor.NEGATIVE, payload=['moderate', info_list[0], 'block'])
    for moderator in moderators:
        query = "INSERT INTO moderation (record_id, moderator_id) VALUES (%s, %s)"
        values = (info_list[0], moderator)
        cursor.execute(query, values)
        connection.commit()
        vk.messages.send(peer_id=moderator,
                         random_id=get_random_id(),
                         attachment=info_list[2],
                         keyboard=keyboard.get_keyboard(),
                         message='@id' + str(info_list[1]))


def save_moderation(event):
    payload = json.loads(event.obj.payload)
    cursor.execute("SELECT source_id FROM rights WHERE source_id = " + str(event.obj.from_id) +
                   " AND acl_type = 1 AND acl_rights = 1")
    result = cursor.fetchone()
    if result:
        if payload[2] == 'block':
            cursor.execute("UPDATE moderation SET decision = '-1' WHERE record_id = " + payload[1] +
                           " AND moderator_id = " + str(event.obj.from_id))
            connection.commit()
        elif payload[2] == 'approve':
            cursor.execute("UPDATE moderation SET decision = '1' WHERE record_id = " + payload[1] +
                           " AND moderator_id = " + str(event.obj.from_id))
            connection.commit()
    else:
        vk.messages.send(peer_id=event.obj.from_id,
                         random_id=get_random_id(),
                         message='Вы не модератор!')


def check_approvement(record_id):
    cursor.execute("SELECT SUM(decision) FROM moderation WHERE record_id = " + record_id)
    result = cursor.fetchone()
    if int(result[0]) <= 0:
        return False
    else:
        return True


def send_to_judges(work_id):
    # generate judges list
    judges = []
    cursor.execute("SELECT source_id FROM rights WHERE acl_type = 2 AND acl_rights = 1")
    judges_rows = cursor.fetchall()
    for entry in judges_rows:
        judges.append(entry[0])

    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('2', color=VkKeyboardColor.NEGATIVE, payload=['judge', work_id, '2'])
    keyboard.add_button('3', color=VkKeyboardColor.DEFAULT, payload=['judge', work_id, '3'])
    keyboard.add_button('4', color=VkKeyboardColor.PRIMARY, payload=['judge', work_id, '4'])
    keyboard.add_button('5', color=VkKeyboardColor.POSITIVE, payload=['judge', work_id, '5'])
    cursor.execute("SELECT record_id, attachment_string, source_id FROM audio_records WHERE record_id = " + work_id)
    work_info = cursor.fetchone()


    for judge in judges:
        query = "INSERT INTO grades (record_id, judge_id) VALUES (%s, %s)"
        values = (work_info[0], judge)
        cursor.execute(query, values)
        connection.commit()
        vk.messages.send(peer_id=judge,
                         random_id=get_random_id(),
                         # attachment=','.join(attachments),
                         attachment=work_info[1],
                         keyboard=keyboard.get_keyboard(),
                         message='@id' + str(work_info[2]))


def save_judgement(event):
    payload = json.loads(event.obj.payload)
    work_id = str(payload[1])
    work_rate = payload[2]
    cursor.execute("UPDATE grades SET grade = " + work_rate + " WHERE record_id = " + work_id +
                   " AND judge_id = " + str(event.obj.from_id))
    connection.commit()


def send_works():
    pass


def send_voice(recipient, voice_message_id, text):
    pass


def add_voice_message(event):
    pass


for event in longpoll.listen():
    if event.type == VkBotEventType.MESSAGE_NEW:
        print(event.obj)
        if event.obj['attachments'] and event.obj['attachments'][0]['audio_message']:
            send_to_moderators(save_work(event))
        elif event.obj.payload:
            payload = json.loads(event.obj.payload)
            if payload[0] == 'moderate':
                save_moderation(event)
                record_id = payload[1]
                if check_approvement(record_id):
                    send_to_judges(record_id)
            elif payload[0] == 'judge':
                save_judgement(event)
        elif event.obj.text == 'заявки':
            send_works()
        else:
            vk.messages.send(peer_id=event.obj['from_id'],
                             random_id=get_random_id(),
                             attachment='doc-185760866_518476614',
                             message='Отправьте нам голосовое сообщение! Ваша заявка будет рассмотрена в ближайшее время')